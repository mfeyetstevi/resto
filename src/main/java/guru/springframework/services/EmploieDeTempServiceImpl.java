package guru.springframework.services;


import guru.springframework.domain.EmploieDeTemp;
import guru.springframework.domain.EmploieDeTemp;
import guru.springframework.repositories.EmploieDeTempRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by jt on 1/10/17.
 */
@Service
public class EmploieDeTempServiceImpl implements EmploieDeTempService {

    private EmploieDeTempRepository productRepository;
   

    @Autowired
    public EmploieDeTempServiceImpl(EmploieDeTempRepository productRepository) {
        this.productRepository = productRepository;
        
    }


    @Override
    public List<EmploieDeTemp> listAll() {
        List<EmploieDeTemp> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add); //fun with Java 8
        return products;
    }


    @Override
    public EmploieDeTemp saveOrUpdate(EmploieDeTemp product) {
        productRepository.save(product);
        return product;
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);

    }

    @Override
    public EmploieDeTemp saveOrUpdateSosuserForm(EmploieDeTemp productForm) {
        EmploieDeTemp savedSosuser = saveOrUpdate(productForm);

        System.out.println("Saved Sosuser Id: " + savedSosuser);
        return savedSosuser;
    }
    
    

}
