package guru.springframework.services;


import guru.springframework.domain.Facture;

import java.util.List;

/**
 * Created by jt on 1/10/17.
 */
public interface FactureService {

    List<Facture> listAll();

  

    Facture saveOrUpdate(Facture product);

    void delete(Integer id);

    Facture saveOrUpdateSosuserForm(Facture productForm);
 
}
