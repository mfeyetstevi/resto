package guru.springframework.services;


import guru.springframework.domain.Facture;
import guru.springframework.repositories.FactureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by jt on 1/10/17.
 */
@Service
public class FactureServiceImpl implements FactureService {

    private FactureRepository productRepository;
   

    @Autowired
    public FactureServiceImpl(FactureRepository productRepository) {
        this.productRepository = productRepository;
        
    }


    @Override
    public List<Facture> listAll() {
        List<Facture> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add); //fun with Java 8
        return products;
    }


    @Override
    public Facture saveOrUpdate(Facture product) {
        productRepository.save(product);
        return product;
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);

    }

    @Override
    public Facture saveOrUpdateSosuserForm(Facture productForm) {
        Facture savedSosuser = saveOrUpdate(productForm);

        System.out.println("Saved Sosuser Id: " + savedSosuser);
        return savedSosuser;
    }
    
    

}
