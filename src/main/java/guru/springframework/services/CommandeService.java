package guru.springframework.services;


import guru.springframework.domain.Commande;

import java.util.List;

/**
 * Created by jt on 1/10/17.
 */
public interface CommandeService {

    List<Commande> listAll();

  Commande get (Integer id);

    Commande saveOrUpdate(Commande product);

    void delete(Integer id);

    Commande saveOrUpdateSosuserForm(Commande productForm);
 
}
