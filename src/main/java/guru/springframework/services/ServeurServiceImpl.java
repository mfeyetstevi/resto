package guru.springframework.services;


import guru.springframework.domain.Serveur;
import guru.springframework.repositories.ServeurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Created by jt on 1/10/17.
 */
@Service
public class ServeurServiceImpl implements ServeurService {

    private ServeurRepository productRepository;
   

    @Autowired
    public ServeurServiceImpl(ServeurRepository productRepository) {
        this.productRepository = productRepository;
        
    }


    @Override
    public List<Serveur> listAll() {
        List<Serveur> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add); //fun with Java 8
        return products;
    }


    @Override
    public Serveur saveOrUpdate(Serveur product) {
        productRepository.save(product);
        return product;
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);

    }

    @Override
    public Serveur saveOrUpdateSosuserForm(Serveur productForm) {
        Serveur savedSosuser = saveOrUpdate(productForm);

        System.out.println("Saved Sosuser Id: " + savedSosuser);
        return savedSosuser;
    }

    @Override
    public Serveur get(Integer id) {
        
      return  productRepository.findById(id).get();
        
    }
    
    

}
