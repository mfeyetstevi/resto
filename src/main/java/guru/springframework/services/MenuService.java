package guru.springframework.services;


import guru.springframework.domain.Menu;

import java.util.List;

/**
 * Created by jt on 1/10/17.
 */
public interface MenuService {

    List<Menu> listAll();

  

    Menu saveOrUpdate(Menu product);

    void delete(Integer id);

    Menu saveOrUpdateSosuserForm(Menu productForm);
 
}
