package guru.springframework.services;


import guru.springframework.domain.Commande;
import guru.springframework.repositories.CommandeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by jt on 1/10/17.
 */
@Service
public class CommandeServiceImpl implements CommandeService {

    private CommandeRepository productRepository;
   

    @Autowired
    public CommandeServiceImpl(CommandeRepository productRepository) {
        this.productRepository = productRepository;
        
    }


    @Override
    public List<Commande> listAll() {
        List<Commande> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add); //fun with Java 8
        return products;
    }


    @Override
    public Commande saveOrUpdate(Commande product) {
        productRepository.save(product);
        return product;
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);

    }

    @Override
    public Commande saveOrUpdateSosuserForm(Commande productForm) {
        Commande savedSosuser = saveOrUpdate(productForm);

        System.out.println("Saved Sosuser Id: " + savedSosuser.getId());
        return savedSosuser;
    }

    @Override
    public Commande get(Integer id) {
       return productRepository.findById(id).get();
    }
    
    

}
