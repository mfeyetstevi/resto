package guru.springframework.services;


import guru.springframework.domain.Menu;
import guru.springframework.repositories.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by jt on 1/10/17.
 */
@Service
public class MenuServiceImpl implements MenuService {

    private MenuRepository productRepository;
   

    @Autowired
    public MenuServiceImpl(MenuRepository productRepository) {
        this.productRepository = productRepository;
        
    }


    @Override
    public List<Menu> listAll() {
        List<Menu> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add); //fun with Java 8
        return products;
    }


    @Override
    public Menu saveOrUpdate(Menu product) {
        productRepository.save(product);
        return product;
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);

    }

    @Override
    public Menu saveOrUpdateSosuserForm(Menu productForm) {
        Menu savedSosuser = saveOrUpdate(productForm);

        System.out.println("Saved Sosuser Id: " + savedSosuser);
        return savedSosuser;
    }
    
    

}
