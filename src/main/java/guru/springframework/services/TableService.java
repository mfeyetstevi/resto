package guru.springframework.services;


import guru.springframework.domain.Table_Rest;

import java.util.List;

/**
 * Created by jt on 1/10/17.
 */
public interface TableService {

    List<Table_Rest> listAll();

  Table_Rest get (Integer id);

    Table_Rest saveOrUpdate(Table_Rest product);

    void delete(Integer id);

    Table_Rest saveOrUpdateSosuserForm(Table_Rest productForm);
 
}
