package guru.springframework.services;


import guru.springframework.domain.Serveur;

import java.util.List;

/**
 * Created by jt on 1/10/17.
 */
public interface ServeurService {

    List<Serveur> listAll();

  Serveur get(Integer id);

    Serveur saveOrUpdate(Serveur product);

    void delete(Integer id);

    Serveur saveOrUpdateSosuserForm(Serveur productForm);
 
}
