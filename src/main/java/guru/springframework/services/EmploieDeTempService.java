package guru.springframework.services;


import guru.springframework.domain.EmploieDeTemp;

import java.util.List;

/**
 * Created by jt on 1/10/17.
 */
public interface EmploieDeTempService {

    List<EmploieDeTemp> listAll();

  

    EmploieDeTemp saveOrUpdate(EmploieDeTemp product);

    void delete(Integer id);

    EmploieDeTemp saveOrUpdateSosuserForm(EmploieDeTemp productForm);
 
}
