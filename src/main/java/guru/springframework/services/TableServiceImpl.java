package guru.springframework.services;


import guru.springframework.domain.Table_Rest;
import guru.springframework.repositories.TableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by jt on 1/10/17.
 */
@Service
public class TableServiceImpl implements TableService {

    private TableRepository productRepository;
   

    @Autowired
    public TableServiceImpl(TableRepository productRepository) {
        this.productRepository = productRepository;
        
    }


    @Override
    public List<Table_Rest> listAll() {
        List<Table_Rest> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add); //fun with Java 8
        return products;
    }


    @Override
    public Table_Rest saveOrUpdate(Table_Rest product) {
        productRepository.save(product);
        return product;
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);

    }

    @Override
    public Table_Rest saveOrUpdateSosuserForm(Table_Rest productForm) {
        Table_Rest savedSosuser = saveOrUpdate(productForm);

        System.out.println("Saved Sosuser Id: " + savedSosuser);
        return savedSosuser;
    }

    @Override
    public Table_Rest get(Integer id) {
      return productRepository.findById(id).get();
    }
    
    

}
