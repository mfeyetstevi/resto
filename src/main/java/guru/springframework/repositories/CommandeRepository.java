package guru.springframework.repositories;

import guru.springframework.domain.Commande;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
/**
 * Created by jt on 1/10/17.
 */
public interface CommandeRepository extends CrudRepository<Commande, Integer> {
   
  
    
    
}
