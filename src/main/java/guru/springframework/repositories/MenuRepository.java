package guru.springframework.repositories;

import guru.springframework.domain.Menu;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
/**
 * Created by jt on 1/10/17.
 */
public interface MenuRepository extends CrudRepository<Menu, Integer> {
   
  
    
    
}
