package guru.springframework.repositories;

import guru.springframework.domain.EmploieDeTemp;
import guru.springframework.domain.Facture;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
/**
 * Created by jt on 1/10/17.
 */
public interface FactureRepository extends CrudRepository<Facture, Integer> {
   
  
    
    
}
