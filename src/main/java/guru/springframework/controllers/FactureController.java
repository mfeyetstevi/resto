package guru.springframework.controllers;



import guru.springframework.domain.Facture;
import guru.springframework.domain.Table_Rest;
import guru.springframework.services.CommandeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import guru.springframework.services.FactureService;
import guru.springframework.services.ServeurService;
import guru.springframework.services.TableService;


/**
 * Created by jt on 1/10/17.
 */
@RestController
public class FactureController {
    private FactureService productService;
     private CommandeService commandeService;
private TableService tableService;
   



    @Autowired
    public void setSosuserService(FactureService productService) {
        this.productService = productService;
    }

  @RequestMapping({"/facture/list"})
    public List<Facture> listFacture(Model model){
        
        return productService.listAll();
    }



    @RequestMapping(value ="/facture/new/{cmd}")
    public Facture neweditcommande(@PathVariable("cmd") Integer cmd){
        Facture s= new Facture();
        s.setFacture_id(Integer.MIN_VALUE);
        s.setCommande_id(commandeService.get(cmd));

        
        return productService.saveOrUpdate(s);
    }

    @RequestMapping(value="/facture/delete/{id}")
    public boolean deetecmd(@PathVariable("id") Integer name){
       boolean t=true;
      
               
               productService.delete(name);
        
        
        
        
        return t;
    }
    
}
