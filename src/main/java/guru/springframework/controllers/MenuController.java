package guru.springframework.controllers;



import guru.springframework.domain.Menu;
import guru.springframework.domain.Table_Rest;
import guru.springframework.services.CommandeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import guru.springframework.services.MenuService;
import guru.springframework.services.ServeurService;
import guru.springframework.services.TableService;
import java.math.BigDecimal;


/**
 * Created by jt on 1/10/17.
 */
@RestController
public class MenuController {
    private MenuService productService;
     private CommandeService commandeService;
private TableService tableService;
   



    @Autowired
    public void setSosuserService(MenuService productService) {
        this.productService = productService;
    }

  @RequestMapping({"/menu/list"})
    public List<Menu> listMenu(Model model){
        
        return productService.listAll();
    }



    @RequestMapping(value ="/menu/new/{nom}/{amt}/{cmd}")
    public Menu neweditcommande(@PathVariable("cmd") Integer cmd,@PathVariable("nom") String nom,@PathVariable("amt") BigDecimal amt){
        Menu s= new Menu();
        
        s.setCommande_id(commandeService.get(cmd));
s.setNommenu(nom);
s.setAmount(amt);
        
        return productService.saveOrUpdate(s);
    }

    @RequestMapping(value="/menu/delete/{id}")
    public boolean deetecmd(@PathVariable("id") Integer name){
       boolean t=true;
      
               
               productService.delete(name);
        
        
        
        
        return t;
    }
    
}
