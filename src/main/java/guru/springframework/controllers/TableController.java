package guru.springframework.controllers;



import guru.springframework.domain.Table_Rest;

import guru.springframework.services.CommandeService;
import guru.springframework.services.ServeurService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import guru.springframework.services.TableService;
import guru.springframework.services.TableService;
import guru.springframework.services.TableService;
import java.math.BigDecimal;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * Created by jt on 1/10/17.
 */
@Controller
public class TableController {
    private TableService productService;
     private CommandeService commandeService;
private ServeurService serveurService;
   



    @Autowired
    public void setSosuserService(TableService productService) {
        this.productService = productService;
    }

  @RequestMapping({"/table/list"})
    public String listTable(Model model){
        model.addAttribute("products",productService.listAll());
        return "/product/list";
    }

@RequestMapping(value = "/table/{id}")
    public String editTable(@PathVariable("id") Integer categorieId, Model model) {
        model.addAttribute("productForm", productService.get(categorieId));
         return "/product/tableeditform";
    }

    @RequestMapping(value ="/table/new/",method = RequestMethod.POST)
    public String neweditcommande(@ModelAttribute Table_Rest person, Model model){
       
       
        Table_Rest s= new Table_Rest();
        
        s.setStatus(person.getStatus());
        s.setEtat(person.getEtat());
       
        s.setNom(person.getNom());
 
        
         productService.saveOrUpdate(s);
         model.addAttribute("products",productService.listAll());
         return "/product/list";
    }
    @RequestMapping({"/table/new"})
    public String neweditcommande(Model model){
        model.addAttribute("productForm",new Table_Rest());
        return "/product/tableform";
    }

    @RequestMapping(value="/table/delete/{id}")
    public boolean deetecmd(@PathVariable("id") Integer name){
       boolean t=true;
      
               
               productService.delete(name);
        
        
        
        
        return t;
    }
     @RequestMapping(value ="/table/edit/",method = RequestMethod.POST)
    public String newedit(@ModelAttribute Table_Rest person, Model model){
       
       
       
        
       
 
        
         productService.saveOrUpdate(person);
         model.addAttribute("products",productService.listAll());
         return "/product/list";
    }  
}
