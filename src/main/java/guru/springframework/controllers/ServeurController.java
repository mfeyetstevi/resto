package guru.springframework.controllers;



import guru.springframework.domain.Serveur;
import guru.springframework.domain.Table_Rest;
import guru.springframework.services.CommandeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import guru.springframework.services.ServeurService;
import guru.springframework.services.ServeurService;
import guru.springframework.services.TableService;
import java.math.BigDecimal;


/**
 * Created by jt on 1/10/17.
 */
@RestController
public class ServeurController {
    private ServeurService productService;
     private CommandeService commandeService;
private TableService tableService;
   



    @Autowired
    public void setSosuserService(ServeurService productService) {
        this.productService = productService;
    }

  @RequestMapping({"/serveur/list"})
    public List<Serveur> listServeur(Model model){
        
        return productService.listAll();
    }



    @RequestMapping(value ="/serveur/new/{nom}")
    public Serveur neweditcommande(@PathVariable("nom") String nom){
        Serveur s= new Serveur();
        
        s.setNom(nom);

        
        return productService.saveOrUpdate(s);
    }

    @RequestMapping(value="/serveur/delete/{id}")
    public boolean deetecmd(@PathVariable("id") Integer name){
       boolean t=true;
      
               
               productService.delete(name);
        
        
        
        
        return t;
    }
    
}
