package guru.springframework.controllers;



import guru.springframework.domain.Commande;
import guru.springframework.domain.Table_Rest;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import guru.springframework.services.CommandeService;
import guru.springframework.services.ServeurService;
import guru.springframework.services.TableService;


/**
 * Created by jt on 1/10/17.
 */
@RestController
public class CommandeController {
    private CommandeService productService;
     private ServeurService serveurService;
private TableService tableService;
   



    @Autowired
    public void setSosuserService(CommandeService productService) {
        this.productService = productService;
    }

  @RequestMapping({"/resto/list", "/listusers"})
    public List<Commande> listCommande(Model model){
        
        return productService.listAll();
    }



    @RequestMapping(value ="/resto/new/{serveur}/{table_id}/{status_cuisine}/{status_commande}")
    public Commande neweditcommande(@PathVariable("serveur") Integer serveur,@PathVariable("table_id") Integer table_id,@PathVariable("status_cuisine") String status_cuisine,@PathVariable("status_commande") String status_commande){
        Commande s= new Commande();
        s.setId(Long.MIN_VALUE);
        
        s.setServeur(serveurService.get(serveur));
        s.setStatus_commande(status_commande);
        s.setStatus_cuisine(status_cuisine);
        s.setTable_id(tableService.get(table_id));
        
        return productService.saveOrUpdate(s);
    }

    @RequestMapping(value="/resto/deletecommande/{id}")
    public boolean deetecmd(@PathVariable("id") Integer name){
       boolean t=true;
      
               
               productService.delete(name);
        
        
        
        
        return t;
    }
    
}
