package guru.springframework.controllers;



import guru.springframework.domain.EmploieDeTemp;
import guru.springframework.domain.Table_Rest;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import guru.springframework.services.EmploieDeTempService;
import guru.springframework.services.ServeurService;
import guru.springframework.services.TableService;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by jt on 1/10/17.
 */
@RestController
public class EmploieDeTempController {
    private EmploieDeTempService productService;
     private ServeurService serveurService;
private TableService tableService;
   



    @Autowired
    public void setSosuserService(EmploieDeTempService productService) {
        this.productService = productService;
    }

  @RequestMapping({"/resto/listemp"})
    public List<EmploieDeTemp> listEmploieDeTemp(Model model){
        
        return productService.listAll();
    }



    @RequestMapping(value ="/emp/new/{hdebut}/{hfin}/{date}/{server}")
    public EmploieDeTemp neweditcommande(@PathVariable("serveur") Integer serveur,@PathVariable("hdebut") String hdebut,@PathVariable("hfin") String hfin,@PathVariable("date") String date) throws Exception{
        EmploieDeTemp s= new EmploieDeTemp();
        s.setId_emploiedetemps(Integer.MIN_VALUE);
        

        s.setServeur_id(serveurService.get(serveur));
        s.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(date));
        s.setHdebut(new SimpleDateFormat("dd/MM/yyyy").parse(hdebut));
        s.setHdebut(new SimpleDateFormat("dd/MM/yyyy").parse(hfin));
        
        
        return productService.saveOrUpdate(s);
    }

    @RequestMapping(value="/emp/delete/{id}")
    public boolean deetecmd(@PathVariable("id") Integer name){
       boolean t=true;
      
               
               productService.delete(name);
        
        
        
        
        return t;
    }
    
}
