package guru.springframework.commands;



import java.math.BigDecimal;

/**
 * Created by jt on 1/10/17.
 */
public class AdminForm {
    private Long id;
    private String cniNumber;
 
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCniNumber() {
        return cniNumber;
    }

    public void setCniNumber(String cniNumber) {
        this.cniNumber = cniNumber;
    }


  
}
