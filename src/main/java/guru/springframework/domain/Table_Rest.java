/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guru.springframework.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 *
 * @author isnov-tech-04
 */

@Entity
@Table(name = "table_rest")
public class Table_Rest implements Serializable {
    @Id
      @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Integer table_id;
     @Column
    private String status ;
     @Column
    private String nom ;
      @Column
    private String Etat;
    
    @ManyToOne
    private Serveur server_id;

    public Integer getTable_id() {
        return table_id;
    }

    public void setTable_id(Integer table_id) {
        this.table_id = table_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEtat() {
        return Etat;
    }

    public void setEtat(String Etat) {
        this.Etat = Etat;
    }

    public Serveur getServer_id() {
        return server_id;
    }

    public void setServer_id(Serveur server_id) {
        this.server_id = server_id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
}
