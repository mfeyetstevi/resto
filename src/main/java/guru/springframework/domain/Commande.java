/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package guru.springframework.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import guru.springframework.domain.Table_Rest;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author MFEYET Daniel Steven
 */

@Entity
public class Commande implements Serializable {
    private static final long serialVersionUID = 1L;
   
    @Id
      @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String status_cuisine;
  
   @Column
    private String status_commande;
    
 

    
    @ManyToOne
    private Table_Rest table_id;
 
    @ManyToOne
    private Serveur serveur;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus_cuisine() {
        return status_cuisine;
    }

    public void setStatus_cuisine(String status_cuisine) {
        this.status_cuisine = status_cuisine;
    }

    public String getStatus_commande() {
        return status_commande;
    }

    public void setStatus_commande(String status_commande) {
        this.status_commande = status_commande;
    }



    public Table_Rest getTable_id() {
        return table_id;
    }

    public void setTable_id(Table_Rest table_id) {
        this.table_id = table_id;
    }

    public Serveur getServeur() {
        return serveur;
    }

    public void setServeur(Serveur serveur) {
        this.serveur = serveur;
    }


    


    
}