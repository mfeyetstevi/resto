/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guru.springframework.domain;

import java.security.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author isnov-tech-04
 */
@Entity
public class EmploieDeTemp {
    
    @Id
      @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_emploiedetemps;
    
     @Column
    private Date hdebut;
     @Column
    private Date hfin;
     @Column
    private Date date;
     @Column
   
    private Serveur serveur_id;

    public Integer getId_emploiedetemps() {
        return id_emploiedetemps;
    }

    public void setId_emploiedetemps(Integer id_emploiedetemps) {
        this.id_emploiedetemps = id_emploiedetemps;
    }

    public Date getHdebut() {
        return hdebut;
    }

    public void setHdebut(Date hdebut) {
        this.hdebut = hdebut;
    }

    public Date getHfin() {
        return hfin;
    }

    public void setHfin(Date hfin) {
        this.hfin = hfin;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    

    public Serveur getServeur_id() {
        return serveur_id;
    }

    public void setServeur_id(Serveur serveur_id) {
        this.serveur_id = serveur_id;
    }
    
    
    
    
}
