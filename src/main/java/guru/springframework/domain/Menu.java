/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guru.springframework.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author isnov-tech-04
 */
@Entity
public class Menu implements Serializable {
    
    
    
    @Id
      @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_menu;
     @Column
    private String nommenu;
    
      @Column
    private BigDecimal Amount;
    
    @ManyToOne
    private Commande commande_id;

    public Integer getId_menu() {
        return id_menu;
    }

    public void setId_menu(Integer id_menu) {
        this.id_menu = id_menu;
    }

    public String getNommenu() {
        return nommenu;
    }

    public void setNommenu(String nommenu) {
        this.nommenu = nommenu;
    }

    public BigDecimal getAmount() {
        return Amount;
    }

    public void setAmount(BigDecimal Amount) {
        this.Amount = Amount;
    }

    public Commande getCommande_id() {
        return commande_id;
    }

    public void setCommande_id(Commande commande_id) {
        this.commande_id = commande_id;
    }
    
}
