/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guru.springframework.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author isnov-tech-04
 */
@Entity

public class Serveur implements Serializable {
    
    
    @Id
      @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_serveur;
     @Column
    private String nom;

    public Serveur(Integer id_serveur, String nom) {
        this.id_serveur = id_serveur;
        this.nom = nom;
    }

    public Serveur() {
      
    }

    public Integer getId_serveur() {
        return id_serveur;
    }

    public void setId_serveur(Integer id_serveur) {
        this.id_serveur = id_serveur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Serveur{" + "nom=" + nom + '}';
    }
  
    
    
}
