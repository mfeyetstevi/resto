/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guru.springframework.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author isnov-tech-04
 */
@Entity
public class Facture implements Serializable {
    
    @Id
    private Integer facture_id;
    
    @ManyToOne
    private Commande commande_id;

    public Integer getFacture_id() {
        return facture_id;
    }

    public void setFacture_id(Integer facture_id) {
        this.facture_id = facture_id;
    }

    public Commande getCommande_id() {
        return commande_id;
    }

    public void setCommande_id(Commande commande_id) {
        this.commande_id = commande_id;
    }
    
    
}
